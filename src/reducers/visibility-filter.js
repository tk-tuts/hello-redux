import {VisibilityFilters} from '../constants/visibility-filters';
import {SET_VISIBILITY_FILTER} from '../constants/action-types';

export default function visibilityFilter(state = VisibilityFilters.SHOW_ALL, action) {
    switch (action.type) {
        case SET_VISIBILITY_FILTER: {
            return action.filter;
        }
        default:
            return state;
    }
};
