import * as types from '../constants/action-types';

// action creators
export function addTodo(text) {
    return {
        type: types.ADD_TODO,
        text
    };
};

export const setVisibilityFilter = (filter) => ({
    type: types.SET_VISIBILITY_FILTER,
    filter
});

export const toggleTodo = (id) => {
    return {
        type: types.TOGGLE_TODO,
        id
    };
};
