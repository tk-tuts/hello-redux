import {ADD_TODO, TOGGLE_TODO} from '../constants/action-types';

export default function todos(state = [], action) {
    // handle actions
    switch (action.type) {
        case ADD_TODO: {
            return [
                ...state,
                {
                    text: action.text,
                    completed: false
                }
            ];
        }
        case TOGGLE_TODO: {
            return state.map((todo, index) => {
                if (index === action.id) {
                    return Object.assign({}, todo, {
                        completed: !todo.completed
                    });
                }
                return todo;
            });
        }
        default: {
            // handle actions here
            return state;
        }
    }
};